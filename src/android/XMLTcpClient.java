package com.lebee.plugins;

import android.os.AsyncTask;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class XMLTcpClient extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("xmlRequest")) {
          xmlRequest(data, callbackContext);
          return true;
        }

        return false;
    }

    public void xmlRequest(JSONArray data, final CallbackContext callbackContext) throws JSONException {
       final boolean isSecureConnection = data.getBoolean(0);
       final String host = data.getString(1);
       final short port = (short)data.getInt(2);
       final String root = data.getString(3);
       final int timeout = data.getInt(4);
       final String request = data.getString(5);
       final boolean naiveSSL = data.getBoolean(6);

       // do it in the background :)
       new AsyncTask<Void,Void,Void>() {
           @Override
           protected Void doInBackground(Void... params)  {
               try {
                   String ret = xmlApiRequest(host, port, root, request, timeout, isSecureConnection, naiveSSL);
                   callbackContext.success(ret);
               } catch(SocketTimeoutException e) {
                   callbackContext.error(createErrorResponse("TIMEOUT", e.getMessage()));
               } catch (Exception e) {
                   callbackContext.error(createErrorResponse("OTHER", e.getMessage()));
               }
               return null;
           }
       }.execute();
    }

    public static JSONObject createErrorResponse(String type, String message) {

        try {
            JSONObject ret = new JSONObject();
            ret.put("type", type);
            ret.put("message", message);
            return ret;
        } catch (JSONException e) {
        }

        return null;
    }

    public static String xmlApiRequest(String host, short port, String root, String request,  int timeout, boolean secureConnection, boolean naiveSSL)
      throws  Exception
    {
        return secureConnection ? secureXmlApiRequest(host, port, root, request, timeout, naiveSSL) : plainXmlApiRequest(host, port, root, request, timeout);
    }

    public static String readUntil(String terminator, BufferedReader reader) throws Exception {
        String ret = "";
        // reader buffer.
        int bufferSize = 1024;
        char[] buffer = new char[bufferSize];
        // read.
        while(true){
            // empty buffer.
            Arrays.fill(buffer, (char)0);
            // read from socket (if -1 return something wrong happen)
            int bytesRead = reader.read(buffer, 0, bufferSize);
            if (bytesRead == -1)
                break;
            if (bytesRead > 0) {
                ret += new String(buffer, 0, bytesRead);
            }
            // we are done reading :)
            if (ret.contains(terminator))
                break;
        }
        if (!ret.contains(terminator))
            throw new Exception("DOES.NOT.CONTAIN.TERMINATOR");
        return ret;
    }

    public static SSLSocketFactory GetSSLFactory(boolean ignoreSelfSigned) throws Exception {
        if (ignoreSelfSigned == false)
            return (SSLSocketFactory)SSLSocketFactory.getDefault();
        // create a trust manager.
        TrustManager[] tm = new TrustManager[] { new NaiveTrustManager() };
        // create a ssl context.
        SSLContext ctx = SSLContext.getInstance("TLS");
        // init the context.
        ctx.init(new KeyManager[0], tm, new SecureRandom());
        // return the socket factory.
        return ctx.getSocketFactory();
    }

    public static String secureXmlApiRequest(String host, short port, String root, String request,  int timeout, boolean naiveSSL) throws Exception
    {
        // create a secure socket factory.
        SSLSocketFactory factory = GetSSLFactory(naiveSSL);
        // create a ssl socket.
        SSLSocket clientSocket = (SSLSocket) factory.createSocket();
        // connect.
        clientSocket.connect(new InetSocketAddress(host, port), timeout);
        // set read timeout.
        clientSocket.setSoTimeout(timeout);
        // create a output stream.
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        // create a input stream.
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        // send the request.
        outToServer.writeBytes(request);
        String ret = readUntil("</" + root + ">", inFromServer);
        clientSocket.close();
        return ret;
    }

    public static String plainXmlApiRequest(String host, short port, String root, String request, int timeout) throws Exception
    {
        // create a socket.
        Socket clientSocket = new Socket();
        // connect.
        clientSocket.connect(new InetSocketAddress(host,port), timeout);
        // set read timeout
        clientSocket.setSoTimeout(timeout);
        // create a output stream.
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        // create a input stream.
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        // send the request.
        outToServer.writeBytes(request);
        String ret = readUntil("</" + root + ">", inFromServer);
        clientSocket.close();
        return ret;
    }
}
