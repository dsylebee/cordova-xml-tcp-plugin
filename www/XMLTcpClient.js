/*global cordova, module*/

var defaultOptions = {
    isSecureConnection: true,
    root: 'root',
    timeout: 10000,
    request: null,
    host: null,
    port: null,
    naiveSSL: false
};

module.exports = {
  xmlRequest: function (options, successCallback, errorCallback) {
      var finalOptions = Object.assign({}, defaultOptions, options);
      cordova.exec(successCallback, errorCallback, "XMLTcpClient", "xmlRequest", [
        finalOptions.isSecureConnection,
        finalOptions.host,
        finalOptions.port,
        finalOptions.root,
        finalOptions.timeout,
        finalOptions.request,
        finalOptions.naiveSSL
      ]);
  }
};
